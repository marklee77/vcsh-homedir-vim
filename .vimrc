" use vim settings instead of vi settings
set nocompatible
nnoremap <space> <nop>
let mapleader = ";"

" **********************************************************************
" * vim package manager                                                *
" **********************************************************************
set packpath+=~/.vim/pack/
packloadall

" **********************************************************************
" * syntastic                                                          *
" **********************************************************************
let g:syntastic_ignore_files = ['\.tex$']
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_python_python_exec = 'python3'
let g:syntastic_python_checkers = ['python']

" **********************************************************************
" * gundo.vim                                                          *
" **********************************************************************
let g:gundo_right = 1
let g:gundo_preview_bottom = 1
let g:gundo_help = 0
let g:gundo_prefer_python3 = 1
let g:gundo_close_on_revert = 1
nnoremap <silent> <leader>u :GundoToggle<CR>

" **********************************************************************
" * supertab                                                           *
" **********************************************************************
let b:SuperTabDisabled = 1

" **********************************************************************
" * vim-airline                                                        *
" **********************************************************************
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#fnamemod = ':t'
let g:airline#extensions#tabline#show_tabs = 0
let g:airline_theme = "solarized"
let g:airline_solarized_dark_text = 1
let g:airline_solarized_dark_inactive_border = 1

" **********************************************************************
" * vim-gnupg                                                          *
" **********************************************************************
let g:GPGPreferArmor = 1
let g:GPGPreferSign = 1
let g:GPGDefaultRecipients = ['12AD50B1F2035492687652A39DC39611A390E262']
let g:GPGUsePipes = 1

" **********************************************************************
" * vim-signfify                                                       *
" **********************************************************************
let g:signify_vcs_list = ['git']
let g:signify_realtime = 1
nnoremap <silent> <leader>st :SignifyToggle<CR>
nnoremap <silent> <leader>sh :SignifyToggleHighlight<CR>
nnoremap <silent> <leader>sr :SignifyRefresh<CR>
nnoremap <silent> <leader>sd :SignifyDebug<CR>

" **********************************************************************
" * vim-fugitive                                                       *
" **********************************************************************
nnoremap <silent> <leader>gb :Gblame<CR>
nnoremap <silent> <leader>go :Gbrowse<CR>
nnoremap <silent> <leader>gc :Gcommit<CR>
nnoremap <silent> <leader>gd :Gvdiff<CR>
nnoremap <silent> <leader>gl :Glog<CR>
nnoremap <silent> <leader>gs :Gstatus<CR>
nnoremap <silent> <leader>gw :Gwrite<CR>

" **********************************************************************
" * nerdtree                                                           *
" **********************************************************************
let NERDTreeMinimalUI = 1
nnoremap <silent> <leader>t :NERDTreeToggle<CR>

" **********************************************************************
" * netrw                                                              *
" **********************************************************************
let g:netrw_silent = 1

" **********************************************************************
" * vim-solarized                                                      *
" **********************************************************************
set background=dark
let g:solarized_termcolors = 256
let g:solarized_termtrans = 1
let g:solarized_underline = 0
colorscheme solarized

" **********************************************************************
" * vimtex                                                             *
" **********************************************************************
let g:vimtex_view_method = "zathura"

" **********************************************************************
" * fzf                                                                *
" **********************************************************************
let g:fzf_layout = { 'window': 'enew' }
let g:fzf_history_dir = '~/.local/share/fzf-history'

function! s:update_fzf_colors()
  let rules =
  \ { 'fg':      [['Normal',       'fg']],
    \ 'bg':      [['Normal',       'bg']],
    \ 'hl':      [['Comment',      'fg']],
    \ 'fg+':     [['CursorColumn', 'fg'], ['Normal', 'fg']],
    \ 'bg+':     [['CursorColumn', 'bg']],
    \ 'hl+':     [['Statement',    'fg']],
    \ 'info':    [['PreProc',      'fg']],
    \ 'prompt':  [['Conditional',  'fg']],
    \ 'pointer': [['Exception',    'fg']],
    \ 'marker':  [['Keyword',      'fg']],
    \ 'spinner': [['Label',        'fg']],
    \ 'header':  [['Comment',      'fg']] }
  let cols = []
  for [name, pairs] in items(rules)
    for pair in pairs
      let code = synIDattr(synIDtrans(hlID(pair[0])), pair[1])
      if !empty(name) && code > 0
        call add(cols, name.':'.code)
        break
      endif
    endfor
  endfor
  let s:orig_fzf_default_opts =
        \ get(s:, 'orig_fzf_default_opts', $FZF_DEFAULT_OPTS)
  let $FZF_DEFAULT_OPTS =
        \ s:orig_fzf_default_opts .
        \ empty(cols) ? '' : (' --color='.join(cols, ','))
endfunction

augroup fzf
    au!
    au ColorScheme * call <sid>update_fzf_colors()
augroup end

nnoremap <silent> <leader>o :FZF<CR>

" **********************************************************************
" * colors and highlighting                                            *
" **********************************************************************

" override some solarized highlight colors
augroup solarized
    au!
    au ColorScheme * highlight clear SpellBad
    au ColorScheme * highlight SpellBad ctermfg=red
    au ColorScheme * highlight clear SpellCap
    au ColorScheme * highlight SpellCap ctermfg=yellow
    au ColorScheme * highlight clear SpellRare
    au ColorScheme * highlight clear SpellLocal
    au ColorScheme * highlight SpellLocal ctermfg=green
augroup end

function! CustomHighlights() abort
    highlight CharsOverTextWidth ctermfg=red guifg=red
    highlight gitcommitOverflow  ctermfg=red guifg=red
    highlight BadWhitespace      ctermbg=red guibg=red
endfunction
call CustomHighlights()

function! CustomHighlightMatches() abort
    if &textwidth > 0
        exec 'match CharsOverTextWidth /\m\%>' . &textwidth . 'v.\+/'
    else
        match none
    endif

    2match BadWhitespace /\m\s\+\%#\@<!$\| \+\t\+\|\t\+ \+/
endfunction

augroup customhighlights
    au!
    au ColorScheme       *         call CustomHighlights()
    au VimEnter,WinEnter *         call CustomHighlightMatches()
    au OptionSet         textwidth call CustomHighlightMatches()
augroup end

" **********************************************************************
" * navigate                                                           *
" **********************************************************************
inoremap <silent> <c-h> <esc>:TmuxNavigateLeft<cr>
inoremap <silent> <c-j> <esc>:TmuxNavigateDown<cr>
inoremap <silent> <c-k> <esc>:TmuxNavigateUp<cr>
inoremap <silent> <c-l> <esc>:TmuxNavigateRight<cr>

" **********************************************************************
" * standard vim settings and overrides                                *
" **********************************************************************

" utf-8
set encoding=utf-8
set fileencoding=utf-8

" appearance
set nofoldenable
set nonumber
set noruler
set showcmd
set noshowmode
set nowrap
set scrolloff=4
set laststatus=2
set ambiwidth=single
set showbreak=↪

" default indent
set noautoindent
set nocindent
set nosmartindent

" tabs (spacing)
set shiftwidth=4
set tabstop=4
set expandtab
inoremap <S-Tab> <C-V><Tab>

" formatting
set formatoptions=tcrnql
set textwidth=90

" search
set nohlsearch
set ignorecase
set smartcase
set incsearch

" input behavior
set backspace=indent,eol,start

" backups
set nobackup

" turn off bells
set visualbell
set noerrorbells
set flash

" disable ex mode
nnoremap Q <nop>

" buffers
set hidden
nnoremap <silent> <leader>n :enew<CR>
nnoremap <silent> <leader><Tab> :bnext<CR>
nnoremap <silent> <leader>w
    \ :if len(getbufinfo({'buflisted':1})) == 1 \| q \| else \| bd \| endif<CR>

" windows
nnoremap <silent> <leader>s :split<CR>
nnoremap <silent> <leader>v :vsplit<CR>
nnoremap <silent> <leader>h :vertical resize -5<CR>
nnoremap <silent> <leader>j :resize +5<CR>
nnoremap <silent> <leader>k :resize -5<CR>
nnoremap <silent> <leader>l :vertical resize +5<CR>
nnoremap <silent> <leader>f :only<CR>
nnoremap <silent> <leader>> <C-w>r
nnoremap <silent> <leader>< <C-w>R

" tabs (window layouts)
" nnoremap <silent> <leader>c :tabnew<CR>
nnoremap <silent> <leader>1 1gt
nnoremap <silent> <leader>2 2gt
nnoremap <silent> <leader>3 3gt
nnoremap <silent> <leader>4 4gt
nnoremap <silent> <leader>5 5gt
nnoremap <silent> <leader>6 6gt
nnoremap <silent> <leader>7 7gt
nnoremap <silent> <leader>8 8gt
nnoremap <silent> <leader>9 9gt
nnoremap <silent> <leader>0 10gt
nnoremap <silent> <leader>[ :tabp<CR>
nnoremap <silent> <leader>] :tabn<CR>

" helpers
set spell spelllang=en_us

filetype plugin on
syntax on

" edit vimrc
nnoremap <silent> <leader>ev :vsplit $MYVIMRC<cr>

" set it up so all yanks, and only yanks, copy to system clipboard
function! CopyYank() abort
  if v:event.operator == "y"
    let output = system("cbyank", join(v:event.regcontents, "\n"))
    if v:shell_error
      echoerr output
    endif
  endif
endfunction
autocmd TextYankPost * call CopyYank()

" paste clipboard contents below current line
nnoremap <silent> <leader>p :r!cbput<cr>

" start vimserver if one is not running already
" if empty(v:servername) && exists('*remote_startserver')
"   call remote_startserver('VIM')
" endif

" **********************************************************************
" * file type autocommands                                             *
" **********************************************************************

augroup global
    au!
    " redraw on leaving insert mode
    au InsertLeave * redraw!
augroup end

" binary files
augroup binary
    au!
    au BufNewFile,BufRead *.pdf,*.jpeg,*.jpg,*.gif,*.png silent !open %
    au BufNewFile,BufRead *.pdf,*.jpeg,*.jpg,*.gif,*.png bd
augroup end

augroup clang
    au!
    au FileType c set noexpandtab
    au FileType c set tabstop=8
augroup end

augroup golang
    au!
    au BufNewFile,BufRead *.go set filetype=golang
    au FileType golang set noexpandtab
augroup end

" treat .js/json as javascript
augroup javascript
    au!
    au BufNewFile,BufRead *.js set filetype=javascript
    au BufNewFile,BufRead *.json set filetype=javascript
augroup end

" do not expand tabs or wrap in makefile
augroup makefile
    au!
    au BufNewFile,BufRead [Mm]akefile set noexpandtab nowrap
augroup end

" use pandoc syntax highlighting for markdown
augroup markdown
    au!
    au BufNewFile,BufRead *.md set filetype=pandoc
augroup end

" tex is almost always latex
augroup tex
    au!
    au BufEnter *.tex let g:tex_flavor='latex'
    au BufEnter *.tex nnoremap <silent><buffer> <leader>lc :VimtexCompile<CR>
    au BufEnter *.tex nnoremap <silent><buffer> <leader>lv :VimtexView<CR>
augroup end

" todo
augroup todo
    au!
    au BufNewFile,BufRead todo.txt set filetype=todo
augroup end
